package br.com.boasaude.sogateway.infrastructure.exception;

import br.com.boasaude.sogateway.domain.model.internal.ExceptionMessage;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class IntegrationException extends RuntimeException {

    private HttpStatus httpStatus;
    private ExceptionMessage exceptionMessage;

    public IntegrationException(HttpStatus httpStatus, ExceptionMessage exceptionMessage) {
        this.httpStatus = httpStatus;
        this.exceptionMessage = exceptionMessage;
    }

    public IntegrationException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public IntegrationException(String message, HttpStatus httpStatus, ExceptionMessage exceptionMessage) {
        super(message);
        this.httpStatus = httpStatus;
        this.exceptionMessage = exceptionMessage;
    }
}
