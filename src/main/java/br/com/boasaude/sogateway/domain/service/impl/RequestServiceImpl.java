package br.com.boasaude.sogateway.domain.service.impl;

import br.com.boasaude.sogateway.domain.model.external.so.Request;
import br.com.boasaude.sogateway.domain.model.external.so.Response;
import br.com.boasaude.sogateway.domain.service.RequestService;
import br.com.boasaude.sogateway.infrastructure.api.SoAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final SoAPI soAPI;


    @Override
    public Request save(Request request) {
        return soAPI.save(request);
    }

    @Override
    public Request changeStatus(Long id, Request request) {
        return soAPI.changeStatus(id, request);
    }

    @Override
    public Request findById(Long id) {
        return soAPI.findById(id);
    }
}
