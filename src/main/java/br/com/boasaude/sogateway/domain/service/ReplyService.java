package br.com.boasaude.sogateway.domain.service;

import br.com.boasaude.sogateway.domain.model.external.so.Reply;

public interface ReplyService {

    Reply save(Reply reply);

}
