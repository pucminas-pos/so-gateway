package br.com.boasaude.sogateway.domain.model.external.so;

public enum RequestStatus {

    OPENED, IN_PROGRESS, CLOSED;

}
