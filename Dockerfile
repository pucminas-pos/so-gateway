FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/so-gateway/src
COPY pom.xml /home/so-gateway
RUN mvn -f /home/so-gateway/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/so-gateway/target/so-gateway.jar /usr/local/lib/so-gateway.jar
EXPOSE 8086
ENTRYPOINT ["java","-jar","/usr/local/lib/so-gateway.jar"]