package br.com.boasaude.sogateway.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class RequestCannotBeModifiedException extends IntegrationException {

    public RequestCannotBeModifiedException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
