package br.com.boasaude.sogateway.infrastructure.exception;

import br.com.boasaude.sogateway.domain.model.internal.ExceptionMessage;
import org.springframework.http.HttpStatus;

public class DataDuplicatedException extends IntegrationException {

    public DataDuplicatedException(HttpStatus httpStatus, ExceptionMessage exceptionMessage) {
        super(httpStatus, exceptionMessage);
    }

    public DataDuplicatedException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }

    public DataDuplicatedException(String message, HttpStatus httpStatus, ExceptionMessage exceptionMessage) {
        super(message, httpStatus, exceptionMessage);
    }
}
