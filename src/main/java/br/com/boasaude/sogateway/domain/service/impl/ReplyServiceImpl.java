package br.com.boasaude.sogateway.domain.service.impl;

import br.com.boasaude.sogateway.domain.model.external.so.Reply;
import br.com.boasaude.sogateway.domain.service.ReplyService;
import br.com.boasaude.sogateway.infrastructure.api.SoAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReplyServiceImpl implements ReplyService {

    private final SoAPI soAPI;

    @Override
    public Reply save(Reply reply) {
        return soAPI.save(reply);
    }
}
