package br.com.boasaude.sogateway.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class SoIntegrationException extends IntegrationException {

    public SoIntegrationException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
