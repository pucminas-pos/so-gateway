package br.com.boasaude.sogateway.application.controller;

import br.com.boasaude.sogateway.infrastructure.exception.DataDuplicatedException;
import br.com.boasaude.sogateway.infrastructure.exception.RequestCannotBeModifiedException;
import br.com.boasaude.sogateway.infrastructure.exception.RequestNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity exception(Exception ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(createErrorMap("unexpected error"));
    }

    @ExceptionHandler(DataDuplicatedException.class)
    public ResponseEntity dataIntegrityViolationException(DataDuplicatedException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(RequestCannotBeModifiedException.class)
    public ResponseEntity requestCannotBeModifiedException(RequestCannotBeModifiedException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(RequestNotFoundException.class)
    public ResponseEntity requestNotFoundException(RequestNotFoundException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(createErrorMap(ex.getMessage()));
    }

    private final Map createErrorMap(String message) {
        Map<String, String> map = new HashMap<>();
        map.put("message", message);
        map.put("date", LocalDateTime.now().toString());
        return map;
    }
}
