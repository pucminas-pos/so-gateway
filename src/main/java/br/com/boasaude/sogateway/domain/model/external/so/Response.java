package br.com.boasaude.sogateway.domain.model.external.so;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Response {

    private String protocol;
    private LocalDateTime date;
}
