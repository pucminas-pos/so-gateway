package br.com.boasaude.sogateway.domain.service;

import br.com.boasaude.sogateway.domain.model.external.so.Request;
import br.com.boasaude.sogateway.domain.model.external.so.Response;

public interface RequestService {

    Request save(Request request);

    Request changeStatus(Long id, Request request);

    Request findById(Long id);

}
