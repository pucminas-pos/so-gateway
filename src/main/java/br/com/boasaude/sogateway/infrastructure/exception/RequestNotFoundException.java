package br.com.boasaude.sogateway.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class RequestNotFoundException extends IntegrationException {

    public RequestNotFoundException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
